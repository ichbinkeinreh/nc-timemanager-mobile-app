import '../helper/time_conversion.dart';
import 'abstract_timemanager_object.dart';
import 'client.dart';
import 'project.dart';
import 'task.dart';
import 'timemanager.dart';

class Time extends AbstractTimemanagerObject {
  final DateTime start;
  final DateTime end;
  final String note;
  final String task_uuid;
  final String? paymentStatus;

  Time(
    Timemanager timemanager, {
    String? uuid,
    String? commit,
    DateTime? created,
    DateTime? changed,
    required this.start,
    required this.end,
    required this.note,
    required this.task_uuid,
    this.paymentStatus,
  })  : assert(start != null),
        assert(end != null),
        assert(start == end || start.isBefore(end)),
        assert(task_uuid != null && task_uuid.isNotEmpty),
        assert(note != null),
        super(
          timemanager,
          uuid: uuid,
          commit: commit,
          created: created,
          changed: changed,
        );

  Time.fromMap(Timemanager timemanager, Map<String, dynamic> jsonMap)
      : this(
    timemanager,
          uuid: jsonMap['uuid'],
          commit: jsonMap['commit'],
          created: TimeConversionHelper.jsonToDateTime(jsonMap['created']),
          changed: TimeConversionHelper.jsonToDateTime(jsonMap['changed']),
          start: TimeConversionHelper.jsonToDateTime(jsonMap['start']) ?? DateTime.now(),
          end: TimeConversionHelper.jsonToDateTime(jsonMap['end']) ?? DateTime.now(),
          note: jsonMap['note'] ?? '',
          task_uuid: jsonMap['task_uuid'],
          paymentStatus: jsonMap['paymentStatus'],
        );

  Time.copy(
    CopyBehaviour copyBehaviour,
    Timemanager timemanager,
    Time time, {
    required DateTime start,
    required DateTime end,
    String? note,
    String? task_uuid,
    String? paymentStatus,
  }) : this(
    timemanager,
          uuid: copyBehaviour == CopyBehaviour.createNew ? null : time.uuid,
          commit: copyBehaviour == CopyBehaviour.createNew ? null : time.commit,
          created: copyBehaviour == CopyBehaviour.createNew
              ? DateTime.now()
              : time.created,
          changed: DateTime.now(),
          start: start,
          end: end,
          note: note ?? time.note,
          task_uuid: task_uuid == null ? time.task_uuid : task_uuid,
          paymentStatus: paymentStatus ?? time.paymentStatus,
        );

  @override
  Map<String, dynamic> toMap() => super.toMap()
    ..addAll(
      {
        'start': TimeConversionHelper.dateTimeToJson(start),
        'end':  TimeConversionHelper.dateTimeToJson(end),
        'note': note,
        'task_uuid': task_uuid,
        'paymentStatus': paymentStatus,
      },
    );

  @override
  Set<AbstractTimemanagerObject> get dependentChildObjects => {};
}

extension TimeDuration on Time {
  Duration get duration => end.difference(start);
}

extension RelationExtensions on Time {
  Client get client => timemanager.get<Client>(project.client_uuid)!;

  Project get project => timemanager.get<Project>(task.project_uuid)!;

  Task get task => timemanager.get<Task>(task_uuid)!;
}
