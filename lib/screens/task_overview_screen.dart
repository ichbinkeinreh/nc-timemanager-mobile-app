import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helper/i18n_helper.dart';
import '../model/abstract_timemanager_object.dart';
import '../model/client.dart';
import '../model/project.dart';
import '../model/task.dart';
import '../provider/timemanager_provider.dart';
import '../widgets/app_drawer.dart';
import '../widgets/grid_items/task_grid_item.dart';
import '../widgets/search_grid.dart';

class TaskOverviewScreen extends StatefulWidget {
  static const routeName = '/task/overview';

  @override
  _TaskOverviewScreenState createState() => _TaskOverviewScreenState();
}

class _TaskOverviewScreenState extends State<TaskOverviewScreen> {
  Project? _project;

  Client? get _client => _project!.client;
  var _searchString = '';

  var init = true;

  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);
    if (init) {
      init = false;
      if (ModalRoute.of(context)!.settings.arguments != null) {
        _project = tm.get<Project>(
            ModalRoute.of(context)!.settings.arguments as String)!;
      }
    }
    List<Task> tasks = _project == null
        ? tm.getAll<Task>().toList()
        : _project!.tasks.toList();
    tasks.sort((one, other) =>
        one.name.toLowerCase().compareTo(other.name.toLowerCase()));
    if (_searchString.isNotEmpty) {
      tasks = tasks
          .where(
              (c) => c.name.toLowerCase().contains(_searchString.toLowerCase()))
          .toList();
    }

    return Scaffold(
      drawer: _project == null ? AppDrawer() : null,
      appBar: AppBar(
        title: Text(_project == null
            ? 'general.tasks'.tl(context)
            : _client!.name + ' - ' + _project!.name),
        actions: [
          if (_project != null)
            IconButton(icon: Icon(Icons.add), onPressed: updateTask),
        ],
      ),
      body: SearchGrid(
        childCount: tasks.length,
        builder: (context, i) => TaskGridItem(
          tasks[i].uuid,
          () => updateTask(tasks[i]),
          isRoot: _project == null,
        ),
        onSearchChanged: (s) {
          setState(() {
            _searchString = s;
          });
        },
        onRefresh: () => tm.sync(),
      ),
    );
  }

  void updateTask([Task? task]) async {
    String name = task == null ? '' : task.name;
    await showDialog<String>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(task == null
            ? 'task_screen.create_task'.tl(context)
            : 'task_screen.edit_task'.tl(context)),
        content: Row(
          children: [
            Expanded(
              child: TextFormField(
                initialValue: name,
                decoration: InputDecoration(
                  labelText: 'general.name'.tl(context),
                ),
                onChanged: (value) => name = value,
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              name = '';
              Navigator.of(context).pop();
            },
            child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(MaterialLocalizations.of(context).okButtonLabel),
          ),
        ],
      ),
    );
    if (name.isNotEmpty) {
      final tm = Provider.of<TimemanagerProvider>(context, listen: false);
      if (task == null) {
        tm.create(Task(tm, name: name, project_uuid: _project!.uuid));
      } else {
        tm.update(Task.copy(
          CopyBehaviour.updateCurrent,
          tm,
          task,
          project_uuid: task.project_uuid,
          name: name,
        ));
      }
      tm.sync();
    }
  }
}
