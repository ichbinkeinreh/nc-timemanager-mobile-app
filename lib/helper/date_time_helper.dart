extension DateTimeHelper on DateTime {
  DateTime getStartOfWeek({
    int weekOffset = 0,
    int startOfWeek = DateTime.monday,
  }) {
    int dayOffset = weekday - startOfWeek;
    if (dayOffset < 0) {
      dayOffset += 7;
    }
    dayOffset += weekOffset * -7;
    return subtract(Duration(
      days: dayOffset,
      hours: hour,
      minutes: minute,
      seconds: second,
      milliseconds: millisecond,
      microseconds: microsecond,
    ));
  }

  DateTime getEndOfWeek({
    int weekOffset = 0,
    int startOfWeek = DateTime.monday,
  }) {
    return this
        .getStartOfWeek(weekOffset: weekOffset, startOfWeek: startOfWeek)
        .add(Duration(days: 7))
        .subtract(Duration(milliseconds: 1));
  }

  DateTime getStartOfDay({
    int dayOffset = 0,
  }) {
    return this.subtract(Duration(
      days: -dayOffset,
      hours: hour,
      minutes: minute,
      seconds: second,
      milliseconds: millisecond,
      microseconds: microsecond,
    ));
  }

  DateTime getEndOfDay({
    int dayOffset = 0,
  }) {
    return this
        .getStartOfDay(dayOffset: dayOffset)
        .add(Duration(days: 1))
        .subtract(Duration(milliseconds: 1));
  }
}
