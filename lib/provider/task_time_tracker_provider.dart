import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:nc_timemanager_mobile_app/provider/timemanager_provider.dart';

import '../model/time.dart';

class TaskTimeTrackerProvider with ChangeNotifier {
  static const _keyTimeStarted = 'task_time_tracker_timeStarted';
  static const _keyCurrentTask = 'task_time_tracker_task';
  static const _keyCurrentNote = 'task_time_tracker_note';
  static const _keyPauseStarted = 'task_time_tracker_pauseStarted';
  static const _keyPauseDuration = 'task_time_tracker_pauseDuration';
  final _storage = FlutterSecureStorage();
  bool _localLoaded = false;
  DateTime? _timeStarted;
  DateTime? _pauseStarted;
  Duration _pauseDuration = Duration();
  String? _currentTaskId;
  String? _note;

  TaskTimeTrackerProvider() {
    if (!_localLoaded) {
      _initLoadFromStorage();
    }
  }

  bool get localLoaded => _localLoaded;

  bool get isActive => _currentTaskId != null;

  String? get currentTask => _currentTaskId;

  String? get note => _note;

  bool get paused => _pauseStarted != null;

  set note(String? note) {
    _note = note;
    notifyListeners();
  }

  Duration? get pauseDuration => isActive
      ? _pauseDuration +
          (_pauseStarted != null
              ? DateTime.now().difference(_pauseStarted!)
              : Duration())
      : null;

  Duration get activeSince => isActive
      ? DateTime.now().difference(_timeStarted!) - pauseDuration!
      : DateTime.now().difference(DateTime.now());

  bool start(String taskId, {String? note}) {
    if (_currentTaskId != null) {
      return false;
    }
    _currentTaskId = taskId;
    _timeStarted = DateTime.now();
    _note = note ?? 'Tracked ' + DateFormat.yMd().format(_timeStarted!);
    _pauseDuration = Duration();
    _pauseStarted = null;
    notifyListeners();
    _storage.write(key: _keyCurrentTask, value: _currentTaskId);
    _storage.write(
        key: _keyTimeStarted, value: _timeStarted!.toIso8601String());
    _storage.write(key: _keyCurrentNote, value: _note);
    _storage.write(key: _keyPauseStarted, value: null);
    _storage.write(key: _keyPauseDuration, value: '0');

    return true;
  }

  void pause() {
    if (isActive && !paused) {
      _pauseStarted = DateTime.now();
      notifyListeners();
      _storage.write(
          key: _keyPauseStarted, value: _pauseStarted!.toIso8601String());
    }
  }

  void resume() {
    if (isActive && paused) {
      _pauseDuration = pauseDuration!;
      _pauseStarted = null;
      _storage.write(
          key: _keyPauseDuration, value: _pauseDuration.inSeconds.toString());
      _storage.write(key: _keyPauseStarted, value: null);
    }
  }

  Time? end(TimemanagerProvider tm) {
    if (!isActive) {
      return null;
    }
    var time = Time(
      tm,
      start: _timeStarted!.add(pauseDuration!),
      end: DateTime.now(),
      task_uuid: _currentTaskId!,
      note: _note!,
    );
    _currentTaskId = null;
    _timeStarted = null;
    time = tm.create(time) as Time;
    tm.sync();
    notifyListeners();
    _storage.delete(key: _keyCurrentTask);
    _storage.delete(key: _keyTimeStarted);
    _storage.delete(key: _keyCurrentNote);
    _storage.delete(key: _keyPauseStarted);
    _storage.delete(key: _keyPauseDuration);
    return time;
  }

  bool cancel() {
    if (isActive) {
      _timeStarted = null;
      _currentTaskId = null;
      _note = null;
      notifyListeners();
      _storage.delete(key: _keyCurrentTask);
      _storage.delete(key: _keyTimeStarted);
      _storage.delete(key: _keyCurrentNote);
      _storage.delete(key: _keyPauseStarted);
      _storage.delete(key: _keyPauseDuration);
      return true;
    }
    return false;
  }

  Future<void> _initLoadFromStorage() async {
    _localLoaded = true;
    await Future.delayed(Duration(milliseconds: 100));
    final taskId = await _storage.read(key: _keyCurrentTask);
    final started = await _storage.read(key: _keyTimeStarted);
    final note = await _storage.read(key: _keyCurrentNote);
    final pauseStarted = await _storage.read(key: _keyPauseStarted);
    final pauseDuration = await _storage.read(key: _keyPauseDuration);
    if (taskId != null && started != null && note != null) {
      _timeStarted = DateTime.parse(started);
      _currentTaskId = taskId;
      _note = note;
      _pauseDuration = Duration(seconds: int.parse(pauseDuration!));
      if (pauseStarted != null) {
        _pauseStarted = DateTime.parse(pauseStarted);
      }
      if (_currentTaskId == null) {
        cancel();
      } else {
        notifyListeners();
      }
    }
  }
}
